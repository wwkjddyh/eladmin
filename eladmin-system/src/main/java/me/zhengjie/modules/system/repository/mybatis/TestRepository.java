package me.zhengjie.modules.system.repository.mybatis;

import me.zhengjie.modules.system.domain.mybatis.Test;
import org.springframework.stereotype.Repository;

/**
 * 测试数据
 * @author jianbo
 */
@Repository
public interface TestRepository extends BaseRepository<Test, String> {
}
