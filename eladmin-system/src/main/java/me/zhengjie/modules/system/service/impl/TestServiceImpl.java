package me.zhengjie.modules.system.service.impl;

import me.zhengjie.modules.system.domain.mybatis.Test;
import me.zhengjie.modules.system.service.TestService;
import org.springframework.stereotype.Service;

/**
 * 测试数据基础业务实现
 * @author jianbo
 */
@Service
public class TestServiceImpl extends AbstractService<Test, String> implements TestService {

    @Override
    public Test getTestById(String recordId) {
        return this.getById(recordId);
    }

    @Override
    public void saveTest(Test entity) {
        this.save(entity);
    }

    @Override
    public Test findByRecordId(Test test) {
        return getById(test.getRecordId());
    }

    @Override
    public void deleteById(String id) {
        Test test = new Test();
        test.setRecordId(id);
        this.delete(test);
    }
}
