package me.zhengjie.modules.system.rest;

import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import me.zhengjie.modules.system.domain.mybatis.Test;
import me.zhengjie.modules.system.service.TestService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author jianbo
 */
@RestController
@RequiredArgsConstructor
@Api(tags = "系统：测试")
@RequestMapping("/api/test")
public class TestController {

    @Resource
    private TestService testService;

    @ApiOperation("demo1")
    @GetMapping("demo1")
    public ResponseEntity<Object> demo1() {
        Map<String, Object> data = Maps.newHashMap();
        data.put("key1", "1");
        data.put("key2", "2");
        Test test = new Test();
        data.put("test-data", testService.find(test));
        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}
