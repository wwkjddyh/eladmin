package me.zhengjie.modules.system.domain.mybatis;

import java.io.Serializable;

/**
 * 测试
 * @author jianbo
 */
public class Test implements Serializable {

    private static final long serialVersionUID = 6217407851333306823L;

    /**
     * 唯一标识
     */
    private String recordId;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 信息标识(0,删除;1,正常)
     */
    private Integer flag;

    /**
     * 查询关键字
     */
    private String key;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "Test{" +
                "recordId='" + recordId + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", flag=" + flag +
                '}';
    }

    public Test() {
    }

    public Test(String recordId, String title, String content, Integer flag) {
        this.recordId = recordId;
        this.title = title;
        this.content = content;
        this.flag = flag;
    }
}
