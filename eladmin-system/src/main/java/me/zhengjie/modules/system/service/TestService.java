package me.zhengjie.modules.system.service;

import me.zhengjie.modules.system.domain.mybatis.Test;

/**
 * 测试数据基本业务
 * @author jianbo
 */
public interface TestService extends BaseService<Test, String> {

    /**
     * 根据ID获得数据
     * @param recordId 唯一标识
     * @return Test
     */
    Test getTestById(String recordId);

    /**
     * 保存测试数据
     * @param entity 数据信息
     */
    void saveTest(Test entity);

    /**
     * 根据测试对象(必需有recordId属性值)
     * @param test 测试对象
     * @return Test
     */
    Test findByRecordId(Test test);

    /**
     * 根据唯一标识删除信息
     * @param id 唯一标识
     */
    void deleteById(String id);
}
